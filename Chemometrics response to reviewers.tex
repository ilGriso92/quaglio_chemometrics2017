\documentclass[english]{article}
\usepackage{graphicx, caption, subcaption}
\usepackage[margin=0.8in]{geometry}
\usepackage{lineno,hyperref}
\usepackage{amsmath,mathtools,upgreek}
\usepackage{amssymb, tabto, booktabs}
\usepackage[numbers,sort&compress]{natbib}
\renewcommand{\baselinestretch}{0.8}
\author{M. Quaglio, E. S. Fraga, E. Cao, A. Gavriilidis, F. Galvanin}
\title{A model-based data mining approach for determining the domain of validity of approximated models}
\date{}



\begin{document}
\maketitle\vspace{0cm}
\begin{center}
\textbf{Response to reviewers}
\end{center}
We are thankful to both reviewers for the careful reading of our manuscript and for the useful and thoughtful comments and suggestions. In this document, the comments will be addressed reporting the changes made to the manuscript, if any.
\begin{center}
\textbf{Reviewer 1}
\end{center}
\textit{"The authors assume that residuals  $r_i=G(x_i)-g(x_i)$ are independent Gaussian random variables, where $G(x_i)$ are either experimental data or output of a, possibly too complicated for optimization, rigorous computer model; $g(x_i)$ are output of the approximated but tractable computer model.  Such an assumption is convenient for mathematical analysis but seems not very realistic. Is your method applicable if $r_i$ are caused not by the random measurement errors but by the imprecision of $g(x)$?  This problem should be discussed to clarify the conditions of the applicability of the proposed method."}\newline

\textit{"To estimate the parameters of the considered model the authors apply the method of maximum likelihood (ML) which crucially depends on the assumption that $r_i$ are independent Gaussian random variables. Some arguments explaining the choice of ML should be presented. For example, the method of least squares (LS) is applicable without such a strong assumption about residuals. Generally speaking, the application of both methods (ML and LS) is reduced to a global optimization problem; however, in the case of LS the corresponding global optimization problem is quite well researched, see e.g.  DOI: 10.1007/s10898-011-9840-9."}\newline

We recognise the lack of clarity regarding these aspects (also highlighted by the second reviewer). There are three important quantities that have to be defined to clarify the point. $G(x_i)$, which is an experimental observation; $G^*(x_i)$ is the expected value for the corresponding experimental observation $G(x_i)$ (it may be considered the true value in case of Gaussian noise with zero mean); and $g(x_i)$ that is the output of the approximated tractable computer model. In a conventional parameter estimation problem, it is assumed that model residuals $r_i= G(x_i)-g(x_i)$ and measurement errors $\epsilon_i= G(x_i)-G^*(x_i)$  follow the same distribution. From this assumption, the Maximum Likelihood approach is derived. In the presence of an incorrect model structure, one cannot assume that, but our approach is concerned with modifying the likelihood method to address the problem of parameter estimation through the automated selection of model-compatible experimental data. Model-compatible data represent a subset of the whole dataset available for which the distribution of the model residuals after the parameter estimation becomes indistinguishable from the distribution of measurement errors.\newline In order to improve the clarity of the manuscript the following modifications were made:
\begin{enumerate}
\item{Section \textbf{2. Methodology}: [...]A standard approach for estimating model parameters from experimental data is the least square method. However, least squares approaches do not account properly for measurement noise in the parameter estimation. A more sophisticated method that demonstrated to provide good estimates in a broad range of situations is the maximum likelihood estimator [3]. The method derives from the assumption that it does exist a value of the parameters, namely the maximum likelihood (ML) estimate $\hat{\boldsymbol{\uptheta}}_{\textrm{ML}}$, which maximises the likelihood of observing the experimental data, given the model parametrisation. The computation of the ML estimate is performed through the maximisation of the likelihood function $L$ or, indifferently, its natural logarithm $\Phi_{\textrm{ML}}=\textrm{ln}L$ [3]. A discrepancy between the distribution of model residuals $\hat{y}_{ij}(\hat{\boldsymbol{\uptheta}}_{\textrm{ML}})-y_{ij}$ and the distribution of the measurement errors is interpreted as a consequence of incorrect model specification, and it is normally detected through statistical tests that assess the goodness of fit (e.g. a $\chi^2$-test). [...]}
\item{Section \textbf{2. Methodology}: [...] The method follows from the assumption that any model structure is capable of fitting accurately experimental data as long as the fitted domain is not excessively vast (as an example, any continuous nonlinear function is locally well approximated by a linear model). [...]}
\item{Section \textit{2.1 Model-based data mining for parameter estimation}: The approach illustrated here is proposed with the aim of addressing the problem of parameter estimation through the automated selection of model-compatible experimental data. Model-compatible data represent a subset $\Psi'\subseteq\Psi$ of the whole available dataset such that the fitting of $\Psi'$ leads to a distribution of model residuals that cannot be distinguished from a distribution of measurement errors. The necessity of making an assumption on the distribution of the measurement noise justifies the employment of a ML approach as a starting point for the following derivations. [...]}\newline
\end{enumerate}
\textit{"Prof. E.Fraga is a co-author of DOI: 10.1016/j.compchemeng.2006.02.003 where the multidimensional parametric space was investigated by a method of visualization. I am sure that a discussion on hybridization of the  method considered in the present paper with visualization would be interesting for the readership of CHEMOLAB."}\newline

A paragraph was added at the end of Section \textit{4.2 Domain of expected model reliability} discussing the possibility of a hybridisation of the proposed method together with high dimensional data analysis and space visualisation techniques:
\begin{enumerate}
\setcounter{enumi}{3}
\item{Section \textit{4.2 Domain of expected model reliability}: [...] The identified domain of model reliability may be characterised by a very complicated geometry due to the RBF adopted as kernel for the SVM, but also nonlinearity in both the system and the approximated model. The nonconvexity of the region of reliability may result in the achievement of unreliable solutions when employed to bound model-based optimisation problems. In such context, the employment of visualisation techniques may be beneficial for supporting the identification of Pareto optimal solutions (Zilinskas \emph{et al.} 2006). The hybridisation of the proposed model identification approach together with high dimensional data analysis and space visualisation will be object of future studies.}\newline
\end{enumerate}
\textit{"Approximated models frequently are needed for the optimal design.  In such a case the required precision of the model is higher in the supposed vicinity of a maximizer than elsewhere. Please discuss the needed modification of your method to take into account such requirement."}\newline

Model reliability maps provide useful information to assess the reliability of optimal design points identified through model-based optimisation. However, if an optimal design point is identified outside the domain of model reliability, the computed solution and the model predictions in its neighbourhood shall not be trusted and the approximated model shall be replaced with a more appropriate one. This is a recognised limitation of our approach and it is in our aim to address it in future works. The final part of the conclusion was modified to address the comment:
\begin{enumerate}
\setcounter{enumi}{4}
\item{Section \textbf{5. Conclusion}: [...] If an optimal process point is identified in a region of low model reliability, the computed solution and the model predictions in its neighbourhood shall not be trusted. At the current stage of the study, the proposed approach does not provide guidance on how to modify the model structure to extend the boundaries of the model reliability domain. It will be object of future work to promote further the integration of machine learning technologies and advanced tools for model building for supporting the development of intelligent algorithms for the quick construction, refinement and statistical validation of phenomenological models.}\newline
\end{enumerate}

\begin{center}
\textbf{Reviewer 2}
\end{center}

\textit{"The MBDM step identifies, as stated on page 8, "experiments performed outside the domain of model reliability and experiments in which measurements are affected by excessive error". It would be useful to discuss about the difference between these two categories, with reference to the aim of identifying model-incompatible data."}\newline


\begin{enumerate}
\setcounter{enumi}{5}
\item{Section \textit{2.1 Model-based data mining for parameter estimation:} [...] automated exclusion from the parameter estimation problem of the experiments that are incompatible with the modelling assumptions, i.e.: \textit{1}) experiments performed outside the domain of model reliability and \textit{2}) experiments in which measurements are affected by excessive error. Notice that MBDM does not distinguish between these two categories. In fact MBDM only classifies the experiments based on the associated fitting realised by the candidate model. A possible practical way to provide a more accurate classification of the data in the two aforementioned categories is to repeat the experiments. If the incompatibility persists after the repetition, the experiment shall be classified in the first category, i.e. the trial was performed outside the domain of model reliability. If the repeated experiment is instead found to be compatible, the incompatibility detected before the repetition shall be interpreted as a consequence of an excessive measurement error.}\newline
\end{enumerate}


\textit{"The SVM classifer, as any empirical predictor obtained from data, is reliable only for interpolation, not for extrapolation. How is the validity of this classifier itself taken into account in the proposed approach?"}\newline

We recognise that the classification provided by SVM is purely data driven and depends on the choice of the kernel as well as a proper choice for the values of the hyperparameters $C$ and $\gamma$. We also recognise that an accurate SVM classification relies on the availability of an abundant and distributed training set. Especially at the beginning of the experimental activity, the number of training points may be limited and the classification may be poor. In the proposed approach for model identification, the SVM is trained for quantifying the reliability of a model in the design space. Maps of reliability are then employed to bound the design of additional experiments to improve the model parametrisation. Thus, in the presented approach, an inaccurate classifier would result only in an efficiency issue due to the fact that model identification may require a number of experiments that is higher than optimal. However, the inaccuracy of the SVM does not prevent the identification of an accurate model. In order to clarify how the validity of the classifier is accounted in the proposed procedure, a paragraph was added in the results section:\newline

\begin{enumerate}
\setcounter{enumi}{6}
\item{Section \textit{4.2 Domain of expected model reliability}: The mapping of the design space provided by SVM is purely data driven and depends on the choice of the kernel function as well as the values of the associated hyperparameters (see Section 2.2). Furthermore, an accurate SVM classification requires the availability of a relatively abundant and distributed training set. Especially at the beginning of the experimental activity, the number of training points may be limited and the classification may be poor. However, notice that in the presented approach for model identification (see Figure 1), an inaccurate classification would only impact the efficiency of the method (i.e. the number of experiments required to identify the model) and not the eventual outcome. An initially inaccurate reliability map may lead to the design of incompatible experiments in regions of the design space that are classified as reliable. However, the accuracy of the SVM classifier increases as the experimental activity proceeds and does not prevent the ultimate identification of an accurate model.}
\end{enumerate}

\textit{"The example provided offers a useful illustration of how the proposed approach works, but it does not show how effective the approach is in achieving its goal. Is there a way to extend the example to address this issue?"}\newline

There are two final goals that the we want to achieve through the proposed framework: 1) the identification of a robust parametrisation for an approximated model structure; 2) the diagnosis of the cause of the model-process mismatch. It was not clear to us whether the reviewer was referring to the first or to the second goal, thus we agreed on improving the clarity of the paper on both aspects. Regarding goal 1, it was mentioned that it was not in the aims of this manuscript to detail the 3rd step of the procedure in Figure 1, i.e. the experimental design step. An extended case study to test the overall procedure is going to be object of future research activities. The manuscript was modified as follows:

\begin{enumerate}
\setcounter{enumi}{7}
\item{Section \textit{3.3 Methods}: The experimental design step, illustrated in the proposed methodology in Section 2, will not be considered in the presented case. The design and development of a complete procedure to extended case studies (both in silico and on real setups) is going to be object of future research activities.}
\end{enumerate}
Concerning goal 2, the manuscript was already modified according to the amendment number 5 of the present document.\newline

\textit{"Description of the experiments on page 13 could be done more effectively by using a table."}\newline

Bullet points were substituted with a more readable table.\newline

\textit{"Besides, there are some typos to be corrected:"}\\

\textit{"Page 3, line 4, "In this simuationS""}
\begin{enumerate}
\setcounter{enumi}{8}
\item{[...] In these simulations.}
\end{enumerate}

\textit{"Page 5, line3/4 below equation 3: "all across"?"}
\begin{enumerate}
\setcounter{enumi}{9}
\item{[...] If the model structure is approximated, one shall not expect the model to give good predictions throughout the whole experimental design space and for all its measurable output variables.}
\end{enumerate}


\textit{"Page 7, line 2, "The result is obtained modifying.."?"}\\\\
Rephrased as:
\begin{enumerate}
\setcounter{enumi}{10}
\item{[...] For this purpose, an $N_{m}\textrm{x}N_{exp}$ matrix $\boldsymbol{\Lambda}$ of binary variables $\lambda_{ij}\in\{0,1\}$ is defined and the log-likelihood in Eq. (4) is modified as follows.}
\end{enumerate}


\textit{"Page 9, below equation 5, "Where" should start with a lower case "w". Please check/correct also similar places."}\newline

Corrected throughout the manuscript.\newline

\textit{"Page 14, line 2 from bottom, "gas bulk" or "bulk gas"?"}\newline

Modified to "bulk gas".\newline

\textit{"Page 20, line 2 in Conclusion, "...estimation its parameters..."?"}

\begin{enumerate}
\setcounter{enumi}{11}
\item{[...] estimation of its parameters [...]}
\end{enumerate}




\bibliographystyle{model1-num-names2}
\bibliography{mybibfile}
%\bibliography{myrefs}

\end{document}

